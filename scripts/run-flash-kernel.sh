#!/bin/sh

set -x

sdv=$(which systemd-detect-virt)

mv $sdv /var/tmp
ln -sf /bin/false $sdv
flash-kernel
mv /var/tmp/systemd-detect-virt $sdv
